/**
 * Created by Georg Plaz.
 */
public class Neuron extends MovablePoint {
    private String id;

    public Neuron(String id, double x, double y) {
        super(x, y);
        this.id = id;
    }

    public String getId() {
        return id;
    }
}