/**
 * Created by Georg Plaz.
 */
public class MovablePoint implements Point {
    private double x;
    private double y;

    public MovablePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public MovablePoint(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void move(Point toPoint, double scaling) {
        double dx = (toPoint.getX() - getX()) * scaling;
        double dy = (toPoint.getY() - getY()) * scaling;
        move(dx, dy);
    }

    public void move(double dx, double dy) {
        setX(getX()+dx);
        setY(getY()+dy);
    }

    public void move(Point delta) {
        move(delta.getX(), delta.getY());
    }
}
