import java.awt.*;
import java.util.*;

/**
 * Created by Georg Plaz.
 */
public class VectorQuantisationInstance {
    public static final double DESTINATION_RADIUS = 5;
    public static final Color DESTINATION_OUTER_COLOUR = new Color(51, 34, 136);
//    public static final Color DESTINATION_INNER_COLOUR = new Color(51, 34, 136);
    public static final double NODE_RADIUS = 11;
    public static final Color NODE_COLOUR = new Color(1, 40, 48);

    private ArrayList<Point> targets;
    private final Neurons neurons;
    private Random random = new Random(0);
    private double learningRate;
    private int epochCounter;
    private Painter painter = new Painter();
    private Random colourRandom = new Random(0);
    private Map<Neuron, Color> colours;
    private int initialPointCount;
    private long checkPruneAtEpoch = 0;
    private Solution currentSolution;
    private int epochsAfterPruning = 0;
    private boolean pruning = true;

    public VectorQuantisationInstance(ArrayList<Point> targets, Neurons neurons, int initialPointCount, Random random) {
        this.initialPointCount = initialPointCount;
        this.targets = new ArrayList<>(targets);
        this.neurons = neurons;
        this.random = random;

        learningRate = Main.INITIAL_LEARNING_RATE;
        epochCounter = 0;
        for (int i = 0; i < initialPointCount; i++) {
            Point randomPoint = targets.get(this.random.nextInt(targets.size()));
            neurons.add(randomPoint.getX(), randomPoint.getY());
        }
        colours = new HashMap<>();
        setDistinctColours();

        for (Point destination : targets) {
            painter.add(destination);
        }
    }

    private void setDistinctColours() {
        int i=0;
        for (Neuron neuron : neurons.getNeurons()) {
            float hue = i++;
            hue/=neurons.getNeurons().size();
            float saturation = 90 + colourRandom.nextFloat() * 10;
            float brightness = 50 + colourRandom.nextFloat() * 10;
            colours.put(neuron, Color.getHSBColor(hue, saturation, brightness));
        }
    }

    public void visualize(Graphics2D g, int width, float height) {
        int i = 0;
        for (Point destination : targets) {
            painter.drawCircle(destination, DESTINATION_RADIUS, DESTINATION_OUTER_COLOUR, width, height, g, 3);
//            painter.drawString(String.valueOf(i), destination, width, height, g);
            i++;
        }
        i = 0;
        synchronized (neurons) {
            for (Neuron neuron : neurons.getNeurons()) {
                painter.fillCircle(neuron, NODE_RADIUS, colours.get(neuron), width, height, g);
                painter.drawCircle(neuron, NODE_RADIUS, Color.BLACK, width, height, g, 3);
                painter.drawString(neuron.getId(), neuron.getX(), neuron.getY(), width, height, g);
                i++;
            }
        }
    }

    public void tick() {
        System.out.println("epoch "+epochCounter+" with learning rate "+learningRate);
        boolean checkingPruning = pruning && checkPruneAtEpoch <= epochCounter;
        if (checkingPruning) {
            boolean toPrune = pruneNecessary();
            checkPruneAtEpoch = Math.round(epochCounter + neurons.getNeurons().size()/5.);
            System.out.println("prune check: "+toPrune+". next prune check at "+checkPruneAtEpoch);
            if (toPrune) {
                System.out.println("pruning!");
                prune();
            } else {
                System.out.println("not pruning!");
                pruning = false;
            }
        }

        if (!pruning) {
            learningRate = Main.INITIAL_LEARNING_RATE * Math.exp(-(epochsAfterPruning / 20.));
//            System.out.println("new learning rate "+learningRate);
        }

        Point destination = targets.get(random.nextInt(targets.size()));
        neurons.pull(destination, learningRate);
        epochCounter++;
        if (!pruning) {
            epochsAfterPruning++;
        }
    }

    public boolean pruneNecessary() {
        currentSolution = new Solution(targets, neurons.getNeurons(), painter, colours);
        Map<Point, Double> radii = new HashMap<>();
        if (neurons.getNeurons().size() <= 3) {
            System.out.println("dummy.");
        }
        for (Point center : currentSolution.getTargetsByCenter().keySet()) {
            if (currentSolution.getTargetsByCenter().get(center).size() <= 3) {
                return true;
            }
        }
        for (Point center : currentSolution.getTargetsByCenter().keySet()) {
            double radius = 0;
            for (Point point : currentSolution.getTargetsByCenter().get(center)) {
                radius = Math.max(Util.distance(center, point), radius);
            }
            radii.put(center, radius);
        }

        ArrayList<Point> centers = new ArrayList<>(currentSolution.getTargetsByCenter().keySet());
        for (int i = 0; i < centers.size(); i++) {
            for (int j = i+1; j < centers.size(); j++) {
                Point current = centers.get(i);
                Point other = centers.get(j);
//                Collection<Point> currentTargets = currentSolution.getTargetsByCenter().get(current);
//                Collection<Point> otherTargets = currentSolution.getTargetsByCenter().get(other);
                if (Util.distance(current, other) < 2* (radii.get(current)+radii.get(other))) {
                    return true;
                }
            }
        }
        return false;
    }

    public void prune() {
        if (neurons.getNeurons().size() >= 1) {
            Neuron closestFirst = neurons.getNeurons().get(0);
            if (neurons.getNeurons().size() >= 2) {
                Neuron closestSecond = neurons.getNeurons().get(1);
                if (neurons.getNeurons().size() >= 3) {
                    double shortestDistance = Double.POSITIVE_INFINITY;
                    for (int i = 0; i < neurons.getNeurons().size(); i++) {
                        Neuron firstNeuron = neurons.getNeurons().get(i);
                        for (int j = i + 1; j < neurons.getNeurons().size(); j++) {
                            Neuron secondNeuron = neurons.getNeurons().get(j);
                            double currentDistance = Util.squaredDistance(firstNeuron, secondNeuron);
                            if (currentDistance < shortestDistance) {
                                closestFirst = firstNeuron;
                                closestSecond = secondNeuron;
                                shortestDistance = currentDistance;
                            }
                        }
                    }
                }
                closestSecond.move(closestFirst, 0.5);
            }
            synchronized (neurons) {
                remove(closestFirst);
            }
        }
    }

    public void remove(Neuron neuron) {
        neurons.remove(neuron);
    }

    public int getEpochCounter() {
        return epochCounter;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public Solution getSolution() {
        return new Solution(targets, neurons.getNeurons(), painter, colours);
    }

    public Painter getPainter() {
        return painter;
    }
}
