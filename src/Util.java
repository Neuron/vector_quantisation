import java.util.Collection;

/**
 * Created by Georg Plaz.
 */
public class Util {
    public static <A extends Point> A findClosest(Point toPoint, Collection<? extends A> points) {
        double minDistance = Double.POSITIVE_INFINITY;
        A minPoint = null;
        for (A node : points) {
            double distance = squaredDistance(node, toPoint);
            if (distance < minDistance) {
                minDistance = distance;
                minPoint = node;
            }
        }
        return minPoint;
    }

    public static double squaredDistance(Point first, Point second) {
        double a = first.getX() - second.getX();
        double b = first.getY() - second.getY();
        return a*a + b*b;
    }

    public static double distance(Point first, Point second) {
        return Math.sqrt(squaredDistance(first, second));
    }

    public static Point calculateCenter(Collection<Point> points) {
        MovablePoint summedPoints = new MovablePoint(0, 0);
        for (Point point : points) {
            summedPoints.move(point);
        }
        return new ConstantPoint(summedPoints.getX()/points.size(), summedPoints.getY()/points.size());
    }
}
