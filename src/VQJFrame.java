import java.awt.*;

/**
 * Created by Georg Plaz.
 */
public class VQJFrame extends VideoFrame{
    private final VectorQuantisationPanel panel;

    public VQJFrame(int width, int height) {
        super("Self Organising Map");
        panel = new VectorQuantisationPanel();
        panel.setPreferredSize(new Dimension(width, height));
        panel.setStartingTime(System.currentTimeMillis());
        add(panel);
        pack();

    }

    public VectorQuantisationPanel getPanel() {
        return panel;
    }

    @Override
    public void doDispose() {
        super.doDispose();
//        System.exit(0);
    }
}
