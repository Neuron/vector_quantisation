
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class Neurons {
    private List<Neuron> nodes = new ArrayList<>();
    public void pull(Point toPoint, double learningRate) {
        Neuron closestPoint = Util.findClosest(toPoint, nodes);
        closestPoint.move(toPoint, learningRate);
    }

    public List<Neuron> getNeurons() {
        return nodes;
    }

    public void add(double x, double y) {
        nodes.add(new Neuron(String.valueOf(nodes.size()), x, y));
    }

    public void remove(Neuron neuron) {
        nodes.remove(neuron);
    }
}
