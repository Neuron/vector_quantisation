/**
 * Created by Georg Plaz.
 */
public class ConstantPoint implements Point {
    private double x;
    private double y;

    public ConstantPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public ConstantPoint(Point point) {
        this.x = point.getX();
        this.y = point.getY();
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }
}
