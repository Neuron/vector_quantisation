import javax.swing.*;
import java.awt.*;

/**
 * Created by Georg Plaz.
 */
class VectorQuantisationPanel extends JPanel {
    public static final int LINE_DELTA = 15;
    private VectorQuantisationInstance vectorQuantisationInstance;
    private Solution bestSolution = null;
    private Solution currentSolution = null;
    private int attemptCounter = -1;
    private long startingTime;

    public VectorQuantisationPanel() {
        setOpaque(true);
        setBackground(Main.BACKGROUND_COLOUR);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int linePosition = 0;

//        graphics.setColor(Main.BACKGROUND_COLOUR);
//        graphics.fillRect(0, 0, getWidth(), getHeight());

        if (vectorQuantisationInstance != null) {
            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("attempt: " + attemptCounter, 5, linePosition += LINE_DELTA);

//            graphics.setColor(Main.TEXT_COLOUR);
//            graphics.drawString("time: " + (System.currentTimeMillis() - startingTime) / 1000., 5, linePosition += LINE_DELTA);

            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("epoch: " + vectorQuantisationInstance.getEpochCounter(), 5, linePosition += LINE_DELTA);

            graphics.setColor(Main.TEXT_COLOUR);
            graphics.drawString("learning rate: " + vectorQuantisationInstance.getLearningRate(), 5, linePosition += LINE_DELTA);

            if (bestSolution != null) {
                bestSolution.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()), Main.BEST_SOLUTION_STROKE);
            }
            if (currentSolution != null) {
                currentSolution.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()), Main.CURRENT_SOLUTION_STROKE);
                graphics.setColor(Main.TEXT_COLOUR);
//                graphics.drawString("current solution length: " + currentSolution.getDistance(), 5, linePosition += LINE_DELTA);
            }

            if (bestSolution != null) {
                graphics.setColor(Main.TEXT_COLOUR);
//                graphics.drawString("best solution length: " + bestSolution.getDistance(), 5, linePosition += LINE_DELTA);
            }
            vectorQuantisationInstance.visualize(graphics, (int) (getPreferredSize().getWidth()), (int) (getPreferredSize().getHeight()));
        }
    }


    public void setVectorQuantisationInstance(VectorQuantisationInstance vectorQuantisationInstance) {
        this.vectorQuantisationInstance = vectorQuantisationInstance;
    }

    public void setBestSolution(Solution bestSolution) {
        this.bestSolution = bestSolution;
    }

    public void setCurrentSolution(Solution currentSolution) {
        this.currentSolution = currentSolution;
    }

    public void setAttemptCounter(int attemptCounter) {
        this.attemptCounter = attemptCounter;
    }

    public void setStartingTime(long startingTime) {
        this.startingTime = startingTime;
    }

    //        public void setP(int width) {
//            this.width = width;
//        }
//
//        public void setHeight(int height) {
//            this.height = height;
//        }
}
