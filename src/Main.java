import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Georg Plaz.
 */
public class Main {
    public static final int WIDTH = 1920;
    public static final int HEIGHT = 1080;
    public static final long MAX_MILLISECONDS = (long) (4.9 * 60 * 1000);
    public static final double MIN_LEARNING_RATE = 0.01;
    public static final double INITIAL_LEARNING_RATE = 0.5;

    public static boolean VISUALIZE_CURRENT_SOLUTION = true;
    public static boolean RENDER_VIDEO = true;
    public static boolean VISUALIZE_PROCESS = true;

    public static final int BEST_SOLUTION_STROKE = 3;
    public static final int CURRENT_SOLUTION_STROKE = 1;
    public static final int FPS_UI = 30;
    public static final int FPS_VIDEO = 30;
    public static final int RENDER_UI_DELAY_MILLI = 1000/ FPS_UI;
    public static final int RENDER_VIDEO_DELAY_MILLI = 1000/ FPS_VIDEO;
    public static final Color BACKGROUND_COLOUR = new Color(136, 204, 238);
    public static final Color BEST_SOLUTION_COLOUR = new Color(48, 125, 37);
    public static final Color CURRENT_SOLUTION_COLOUR = new Color(8, 0, 8);
    public static final Color TEXT_COLOUR = Color.BLACK;
    public static final String RENDER_FOLDER = "rendered";
    public static final int VIDEO_EPOCH_DELAY = 1;

    public static void main(String... args) throws Exception {
        Random random = new Random(0);
        long startingTime = System.currentTimeMillis();

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File("testInput21C.txt"))));

        String line;
        ArrayList<Point> destinations = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            String[] splitLine = line.split(",");
            double x = Double.parseDouble(splitLine[0]);
            double y = Double.parseDouble(splitLine[1]);
            destinations.add(new ConstantPoint(x, y));
        }
//        ArrayList<Point> normalizedDestinations = new ArrayList<>();
//        double scaling = Math.max(maxPointX - minX, maxPointY - minPointY);
//        for (Point destination : destinations) {
//            normalizedDestinations.add(new SimplePoint((destination.getX()-minX) / scaling, (destination.getY()-minPointY) / scaling));
//        }
        reader.close();
        int attemptCounter = 0;
        VQJFrame VQJFrame = new VQJFrame(WIDTH, HEIGHT);
        if (VISUALIZE_PROCESS) {
            VQJFrame.setVisible(true);
        }
        String nameBase = "output";
        int counter = 0;
        String nameExtension = ".mp4";
        File file;
        while ((file = new File(RENDER_FOLDER + "/" + nameBase + "_" + (counter) + nameExtension)).exists()) {
            counter++;
        }
        if (RENDER_VIDEO) {
            VQJFrame.startRecording(file, WIDTH, HEIGHT);
        }

        Solution best = null;
        long videoTimePassed = 0;
        while (System.currentTimeMillis() - startingTime < MAX_MILLISECONDS && VQJFrame.isDisplayable()) {
            VQJFrame.getPanel().setAttemptCounter(attemptCounter);
            VectorQuantisationInstance vectorQuantisationInstance = new VectorQuantisationInstance(destinations, new Neurons(), destinations.size(), random);
            VQJFrame.getPanel().setVectorQuantisationInstance(vectorQuantisationInstance);
            if (VISUALIZE_PROCESS) {
                VQJFrame.getPanel().repaint();
            }
            long nextUiRenderTime = System.currentTimeMillis();
            long nextTickVideoRender = 0;
            while (vectorQuantisationInstance.getLearningRate() > MIN_LEARNING_RATE &&
                    System.currentTimeMillis() - startingTime < MAX_MILLISECONDS &&
                    VQJFrame.isDisplayable()) {
                long currentTime = System.currentTimeMillis();

                boolean renderUi = VISUALIZE_PROCESS && currentTime >= nextUiRenderTime;
                boolean renderVideo = RENDER_VIDEO && vectorQuantisationInstance.getEpochCounter() >= nextTickVideoRender;
                if (VISUALIZE_CURRENT_SOLUTION || renderUi || renderVideo) {
//                    Solution currentSolution = vectorQuantisationInstance.getSolution();
//                    VQJFrame.getPanel().setCurrentSolution(currentSolution);
                }
                if (renderUi) {
                    VQJFrame.getPanel().repaint();
                    nextUiRenderTime = currentTime + RENDER_UI_DELAY_MILLI;
                }
                if (renderVideo) {
                    VQJFrame.captureImage(videoTimePassed, TimeUnit.MILLISECONDS);
                    nextTickVideoRender += VIDEO_EPOCH_DELAY;
                    videoTimePassed += RENDER_VIDEO_DELAY_MILLI;
                }
                vectorQuantisationInstance.tick();
            }

            Solution currentSolution = vectorQuantisationInstance.getSolution();
//            VQJFrame.getPanel().setCurrentSolution(currentSolution);

            System.out.println("current solution with distance "+currentSolution.getSummedSquaredDistance() +" has centers..");
            for (Point point : currentSolution.getTargetsByCenter().keySet()) {
                System.out.println("center at "+point.getX()+", "+point.getY()+" with "+ currentSolution.getTargetsByCenter().get(point).size()+" targets.");
            }
            if (best == null || currentSolution.getSummedSquaredDistance() < best.getSummedSquaredDistance()) {
                best = currentSolution;
                System.out.println("found new solution with distance "+best.getSummedSquaredDistance());
            }
            VQJFrame.getPanel().repaint();
            attemptCounter++;
        }
        System.out.println("finished");
        VQJFrame.stopRecording();
    }

}
