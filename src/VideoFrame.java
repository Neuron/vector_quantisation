import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by Georg Plaz.
 */
public class VideoFrame extends JFrame{
    public static final String CLOSING_CONFIRMATION =
            "A video capture is currently running.\n" +
                    "Are you sure you want to close the Window and stop the recording? Your video will be saved.";
    private VideoCapture videoCapture;
    private boolean capturing = false;

    public VideoFrame(String title) {
        super(title);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                if (isCapturing()) {
                    int buttonIndex = JOptionPane.showConfirmDialog(null, CLOSING_CONFIRMATION);
                    if (buttonIndex == 0) {
                        doDispose();
                    }
                } else {
                    doDispose();
                }
            }
        });
    }

    public void doDispose() {
        stopRecording();
        dispose();
    }

    public boolean isCapturing() {
        return capturing;
    }

    public void startRecording(File file, int width, int height) throws AWTException {
        Container container = getContentPane();
        if (container != null && !capturing) {
            capturing = true;
            videoCapture = new VideoCapture();
            videoCapture.start(file, (int)(container.getLocationOnScreen().getX()), (int)(container.getLocationOnScreen().getY()), width, height);
        }
    }

    public void captureImage(long timeElapsed, TimeUnit timeUnit) {
        if (capturing) {
            videoCapture.capture(getContentPane(), timeElapsed, timeUnit);
        }
    }

    public void stopRecording() {
        if (capturing) {
            capturing = false;
            videoCapture.close();
            videoCapture = null;
        }
    }

}
